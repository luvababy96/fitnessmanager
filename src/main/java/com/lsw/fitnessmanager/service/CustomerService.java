package com.lsw.fitnessmanager.service;

import com.lsw.fitnessmanager.entity.PersonalTrainingCustomer;
import com.lsw.fitnessmanager.model.CustomerInfoUpdateRequest;
import com.lsw.fitnessmanager.model.CustomerItem;
import com.lsw.fitnessmanager.model.CustomerRequest;
import com.lsw.fitnessmanager.model.CustomerWeightUpdateRequest;
import com.lsw.fitnessmanager.repository.PersonalTrainingCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final PersonalTrainingCustomerRepository personalTrainingCustomerRepository;

    public void setCustomer(CustomerRequest request) {
      PersonalTrainingCustomer addData = new PersonalTrainingCustomer();
      addData.setCustomerName(request.getCustomerName());
      addData.setCustomerPhone(request.getCustomerPhone());
      addData.setHeight(request.getHeight());
      addData.setWeight(request.getWeight());
      addData.setDateFirst(LocalDateTime.now());

      personalTrainingCustomerRepository.save(addData);

    }

    public List<CustomerItem> getCustomers() {
        List<PersonalTrainingCustomer> originList = personalTrainingCustomerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for(PersonalTrainingCustomer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setHeight(item.getHeight());
            addItem.setWeight(item.getWeight());
            addItem.setDateFirst(item.getDateFirst());
            addItem.setDateLast(item.getDateLast());

            float heightDouble = (item.getHeight() / 100) * (item.getHeight() / 100);
            double bmi = item.getWeight() / heightDouble;

            addItem.setBmi(bmi);

            String bmiResultText = "";
            if(bmi <= 18.5) {
                bmiResultText = "저체중";
            } else if (bmi <= 22.9) {
                bmiResultText = "정상";
            } else if (bmi <= 24.9) {
                bmiResultText = "과체중";
            } else {
                bmiResultText = "비만";
            }
            addItem.setBmiResultName(bmiResultText);

            result.add(addItem);

        }

        return result;
    }

    public void putCustomerInfo(long id, CustomerInfoUpdateRequest request) {
        PersonalTrainingCustomer originData = personalTrainingCustomerRepository.findById(id).orElseThrow();
        originData.setCustomerName(request.getCustomerName());
        originData.setCustomerPhone(request.getCustomerPhone());

        personalTrainingCustomerRepository.save(originData);

    }

    public void putCustomerWeight(long id, CustomerWeightUpdateRequest request) {
        PersonalTrainingCustomer originData = personalTrainingCustomerRepository.findById(id).orElseThrow();
        originData.setWeight(request.getWeight());

        personalTrainingCustomerRepository.save(originData);
    }

    public void putCustomerVisit(long id) {
        PersonalTrainingCustomer orginData = personalTrainingCustomerRepository.findById(id).orElseThrow();
        orginData.setDateLast(LocalDateTime.now());

        personalTrainingCustomerRepository.save(orginData);
    }

}
