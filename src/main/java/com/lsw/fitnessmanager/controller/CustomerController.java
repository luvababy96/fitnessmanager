package com.lsw.fitnessmanager.controller;

import com.lsw.fitnessmanager.model.CustomerInfoUpdateRequest;
import com.lsw.fitnessmanager.model.CustomerItem;
import com.lsw.fitnessmanager.model.CustomerRequest;
import com.lsw.fitnessmanager.model.CustomerWeightUpdateRequest;
import com.lsw.fitnessmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);
        return "OK";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putCustomerInfo(id,request);

        return "OK";
    }

    @PutMapping("/weight/id/{id}")
    public String putCustomerWeight(@PathVariable long id, @RequestBody @Valid CustomerWeightUpdateRequest request) {
        customerService.putCustomerWeight(id, request);

        return "OK";
    }
    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id) {
        customerService.putCustomerVisit(id);

        return "OK";
    }
}
