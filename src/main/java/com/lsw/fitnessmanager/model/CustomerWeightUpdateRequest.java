package com.lsw.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerWeightUpdateRequest {

    @NotNull
    private Float weight;
}
