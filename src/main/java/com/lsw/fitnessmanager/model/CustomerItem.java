package com.lsw.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {

    private Long id;
    private String customerName;
    private String customerPhone;
    private Float height;
    private Float weight;
    private LocalDateTime dateFirst;
    private LocalDateTime dateLast;
    private Double bmi;
    private String bmiResultName;
}
