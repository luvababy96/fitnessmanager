package com.lsw.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;
    @NotNull
    private Float height;
    @NotNull
    private Float weight;


}
