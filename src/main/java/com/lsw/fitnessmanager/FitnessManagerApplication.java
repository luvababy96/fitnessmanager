package com.lsw.fitnessmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitnessManagerApplication.class, args);
    }

}
