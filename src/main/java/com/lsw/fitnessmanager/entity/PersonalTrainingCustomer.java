package com.lsw.fitnessmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PersonalTrainingCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,length = 20)
    private String customerName;
    @Column(nullable = false,length = 20)
    private String customerPhone;
    @Column(nullable = false)
    private Float height;
    @Column(nullable = false)
    private Float weight;
    @Column(nullable = false)
    private LocalDateTime dateFirst;
    private LocalDateTime dateLast; //@Column 이 필요가 없음 - nullable = true는 적을 필요 없기 때문)


}
