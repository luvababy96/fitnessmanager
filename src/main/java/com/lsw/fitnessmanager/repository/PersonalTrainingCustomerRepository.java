package com.lsw.fitnessmanager.repository;

import com.lsw.fitnessmanager.entity.PersonalTrainingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalTrainingCustomerRepository extends JpaRepository<PersonalTrainingCustomer, Long> {
}
